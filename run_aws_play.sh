#!/bin/bash

# Declare associative arrays up-front
declare -A FILES
declare -A FOUND_FILES
declare -A VAULT_ARRAY
declare -A EXPORT_VARIABLE_MAP

# Set constants
FILES=(
	[VAULT]="aws_creds.yml"
	[EC2PY]="ec2.py"
	[REQUIREMENTS]="requirements.yml"
)

# Left side is defined in the credentials file
# Right side is a property of what Ansible/Boto uses (it looks for these)
EXPORT_VARIABLE_MAP=(
	[aws_secret_key]="AWS_SECRET_ACCESS_KEY"
	[aws_access_key]="AWS_ACCESS_KEY_ID"
	[region]="AWS_REGION"
)

ROLES_DIR="roles/"
RAND_PREFIX="$(tr -dc 'A-Za-z0-9' < /dev/urandom | head -c 10)"
VAULT_FIFO="${RAND_PREFIX}_vault_fifo"

# Function defs
find_file() {
	found=
	for i in $(eval echo "{../,}{*_,}${!1}"); do
		[[ -f "$i" ]] && { eval "${1/FILES/FOUND_FILES}=$i"; found+=1; }
	done

	if (( found <= 0 )); then
		local answer
		echo -en "\n${!1} Not found! Continue? [y/n]:"
		read -n1 -r answer
		if [[ "$answer" =~ ^[Yy]$ ]]; then
			return 0
		else
			return 1
		fi
	fi
}

output_files_array() {
	typeset -p FOUND_FILES | sed "s|.*'(\(.*\) )'.*|Using found files: \1|"
}

usage() {
	local tab=$'\t'
	local newl=$'\n'
	cat <<-EOF
		Usage:
		${tab}1. [.|source] $0
		${tab}${tab}- Use to acquire boto variables (aws credentials) into the parent shell
		${tab}${tab}- Adds alias: "run_aws_play" that can be used in place of referencing script directly${newl}
		${tab}2. $0 [ansible|ansible-playbook|<command>] [<options>|<playbook.yml>]
		${tab}${tab}- Run a command or ansible playbook with inherited boto variables${newl}
		${tab}${tab}Example:
		${tab}${tab}${tab}$0 ansible -i "${FILES[EC2PY]}" -m ping ec2
		${tab}${tab}${tab}$0 ansible -m ping ec2 # assumes '-i ec2.py'
		${tab}${tab}${tab}$0 ansible-playbook main.yml # assumes '-i ec2.py'
		${tab}${tab}${tab}$0 main.yml # assumes 'ansible-playbook -i ec2.py'
	EOF
}

get_vault_data() {
	local name value
	if (( "${#VAULT_ARRAY[@]}" == 0 )); then
		read -sp "Vault password: " VAULT_PASS; echo
		(
			mkfifo -Zm0600 "${VAULT_FIFO}" &>/dev/null
			echo "$VAULT_PASS" > "${VAULT_FIFO}" 2>/dev/null &
		)
		while read -r name value; do
			name="${name//ec2_/aws_}"
			eval VAULT_ARRAY[${name//:}]="$value"
		done < <(
			ansible-vault --vault-password-file="${VAULT_FIFO}" \
			view "${FOUND_FILES[VAULT]}" \
			| grep -v -- '---' | grep ":"
		)
		rm "${VAULT_FIFO}"
	fi

	if (( ! "${#VAULT_ARRAY[@]}" > 0 )); then
		echo "Error decrypting or empty file!"
		$RETURNEXIT 1
	fi
}

export_aws_secrets() {
	if [[ ! "${ANSIBLE_HOST_KEY_CHECKING}" ]]; then
		export ANSIBLE_HOST_KEY_CHECKING=False
	fi

	local new_vault_file="${FOUND_FILES[VAULT]##*/}"
	new_vault_file="${new_vault_file//.yml}"
	if [[ ! ( "$AWS_WAS_SOURCED" && "$VIRTUAL_ENV" == "$new_vault_file" ) ]]; then
		for i in "${!EXPORT_VARIABLE_MAP[@]}"; do
				get_vault_data
				if [[ ${VAULT_ARRAY[$i]} ]]; then
					eval export "${EXPORT_VARIABLE_MAP[$i]}=\"\${VAULT_ARRAY[$i]}\""
				fi
		done
	fi
}

is_sourced() {
	[[ "$0" =~ bash$ ]]
}
# End function defs

trap 'popd 2>/dev/null; rm -f "$VAULT_FIFO" 2>/dev/null; unset VAULT_ARRAY' \
	ERR EXIT RETURN

if is_sourced; then
	RETURNEXIT="return"
	eval alias run_aws_play="'$(readlink -e "$BASH_SOURCE")'"
	find_file "FILES[VAULT]" || "$RETURNEXIT" 1

	output_files_array
	export_aws_secrets

	VIRTUAL_ENV="${FOUND_FILES[VAULT]##*/}"
	VIRTUAL_ENV="${VIRTUAL_ENV//.yml}"
	export VIRTUAL_ENV
	export PS1="($VIRTUAL_ENV)$PS1"
	export AWS_WAS_SOURCED=1

	"$RETURNEXIT"
else
	RETURNEXIT="exit"
fi

if [[ ! $1 || $1 =~ \-\-?h(elp)? ]] && ! is_sourced; then
	usage
  "$RETURNEXIT"
fi

command="$(readlink -e "$1")" || command="$(type -P "$1")"
dirname="${command%/*}"
shift

pushd "$dirname"

for i in "${!FILES[@]}"; do
	find_file "FILES[$i]" || "$RETURNEXIT" 1
done

output_files_array
export_aws_secrets

if [[ "${command##*/}" =~ ^(ansible|ansible-playbook) ]]; then
	if [[ ! "$@" =~ i\ "${FOUND_FILES[EC2PY]}" ]]; then
		command="${command} -i ${FOUND_FILES[EC2PY]}"
	fi
elif [[ "${command##*/}" =~ ^.*\.yml$ ]]; then
	command="ansible-playbook -i ${FOUND_FILES[EC2PY]} ${command##*/}"
fi

if [[ -d "$dirname" ]]; then
	if [[ ! -d roles && -f "${FOUND_FILES[REQUIREMENTS]}" ]]; then
		ansible-galaxy install -p "$ROLES_DIR" -r "${FOUND_FILES[REQUIREMENTS]}"
	fi
fi
eval "$command" \"\$@\"
