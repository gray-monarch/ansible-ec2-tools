# ansible-ec2-tools
Some shell scripts to aid with running ansible playbooks with ec2.py (dynamic inventory)

----
#### bootstrap_ansible_ec2.sh 
##### Run me first to setup environment to be compatible with AWS EC2 and Ansible dynamic inventory scripts

Problem: 
 - It's not necessarily obvious where to get the EC2 dynamic inventory scripts to use with ansible.
 - After downloading `ec2.py` and `ec2.ini` ec2.py/boto errors out because the default `ec2.ini` settings require privileges to access RDS and ElastiCache by default
 - boto and ansible python libraries are usually out of date.
 - `ec2.py` is not executable by default

Solution:
 - Automatically download required `ec2.py` and `ec2.ini`
 - `sed` replace settings in `ec2.ini` in order to work with just EC2 IAM permissions 
 - Set `ec2.py` executable
 - `pip install --upgrade` `ansible` and `boto` 
   - TODO: Lock to version in the future
 - Execute a simple `ec2.py --list` test for functionality

----
#### run_aws_play.sh: usage()
##### Multi-use script to source / run an Ansible playbook 
```
Usage:
        1. [.|source] ./run_aws_play.sh
                - Use to acquire boto variables (aws credentials) into the parent shell
                - Adds alias: "run_aws_play" that can be used in place of referencing script directly

        2. ./run_aws_play.sh [ansible|ansible-playbook|<command>] [<options>|<playbook.yml>]
                - Run a command or ansible playbook with inherited boto variables

                Example:
                        ./run_aws_play.sh ansible -i "ec2.py" -m ping ec2
                        ./run_aws_play.sh ansible -m ping ec2 # assumes '-i ec2.py'
                        ./run_aws_play.sh ansible-playbook main.yml # assumes '-i ec2.py'
                        ./run_aws_play.sh main.yml # assumes 'ansible-playbook -i ec2.py'
```
