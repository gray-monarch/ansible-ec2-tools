#!/bin/bash
CREDS_FILE="${PWD##*/}_aws_creds.yml"
FILES=(
	"https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.ini"
	"https://raw.github.com/ansible/ansible/devel/contrib/inventory/ec2.py"
)

DNF_PACKAGES=(
#	"python2-virtualenv"
	"ansible"
	"ansible-lint"
	"python2-boto"
	"python2-boto3"
	"symlinks"
)

LINK_FILES=(
	"${0##*/}"
	"run_aws_play.sh"
)

for ((i=0; i<${#FILES[@]}; i++)); do
	if [[ ! -f "${FILES[i]##*/}" ]]; then
		wget -c "${FILES[i]}"
	fi
done

# This needs to be done if your boto user just has EC2 permissions
# Otherwise running ec2.py will error out
sed -ri -e 's@^#(rds|elasticache)( =)@\1\2@' ec2.ini
chmod +x ec2.py

if hash dnf; then
	if ! rpm -q "${DNF_PACKAGES[@]}" >/dev/null 2>&1; then
		sudo dnf install -y "${DNF_PACKAGES[@]}"
	fi
elif hash apt-get; then
	echo "Debian/Ubuntu not supported yet"; exit
else
	echo "Platform currently unsupported"; exit
fi

for ((i=0; i<${#LINK_FILES[@]}; i++)); do
	if [[ ! -L "${LINK_FILES[i]}" ]]; then
		ln -s "${0%/*}/${LINK_FILES[i]}" "${LINK_FILES[i]}"
	fi
done
symlinks -cdr .

if [[ ! -f "$CREDS_FILE" ]]; then
	read -p "AWS ACCESS KEY: " AWS_ACCESS_KEY
	read -p "AWS SECRET KEY: " AWS_SECRET_KEY
	cat <<-EOF | ansible-vault encrypt > "${CREDS_FILE}"
		---
		aws_access_key: "$AWS_ACCESS_KEY"
		aws_secret_key: "$AWS_SECRET_KEY"
	EOF
fi

echo "All setup!"
